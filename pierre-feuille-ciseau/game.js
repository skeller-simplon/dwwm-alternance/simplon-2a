let players = {
    one: {
        score: 0,
    },
    two: {
        score: 0
    }
}

const choices = [ 'paper', 'rock', 'scissors' ];
const rules = [
    { source: 'paper', target: 'rock' },
    { source: 'rock', target:  'scissors' },
    { source: 'scissors', target: 'paper' }
];

setup();
function setup(){
    players.one.score = 0;
    players.two.score = 0;
    displayScore();
}

document.querySelector("#choiceRock").addEventListener('click', () => {
    checkMatch('rock', choices[Math.floor(Math.random()*choices.length)]);
    checkEndOfGame();
});
document.querySelector("#choicePaper").addEventListener('click', () => {
    checkMatch('paper', choices[Math.floor(Math.random()*choices.length)]);
    checkEndOfGame();
});
document.querySelector("#choiceScissors").addEventListener('click', () => {
    checkMatch('scissors', choices[Math.floor(Math.random()*choices.length)]);
    checkEndOfGame();
});


function checkEndOfGame(){
    if (players.one.score === 3) {
        alert('T\'as gagné ' + players.one.score + ' - ' + players.two.score + ' bg');
        setup();
    }else if (players.two.score === 3) {
        alert('Boloss, t\'as perdu ' + players.one.score + ' - ' + players.two.score + ' contre un dé');
        setup();
    }else{
        displayScore();
    }
}

function displayScore(){
    document.querySelector('#playerOneScore').innerHTML = players.one.score;
    document.querySelector('#playerTwoScore').innerHTML = players.two.score;
}

function traceLogger(text){
    console.log(text);
    let logDiv = document.querySelector("#logs")
    logDiv.innerHTML += '<p>'+ text+ '</p>';
}

function checkMatch(playerOneInput, playerTwoInput){
    let winner = false;
     
    if (playerOneInput !== playerTwoInput) {
        rules.forEach((element) => {
            if (element.source === playerOneInput && element.target === playerTwoInput) {
                traceLogger(playerOneInput +' contre '+playerTwoInput + ' -- Gagné !')
                players.one.score++;
                winner = true;
                return;
            }else if (element.source === playerTwoInput && element.target === playerOneInput) {
                traceLogger(playerOneInput +' contre '+playerTwoInput + ' -- Perdu :(')
                players.two.score++;
                winner = true;
                return;       
            }
        });
        if (!winner) {
            traceLogger('Erreur: Pas trouvé de gagnant')
        }
    }
    if (!winner) {
        traceLogger(playerOneInput +' contre '+playerTwoInput +  ' -- Egalité')
    }
};

function resetGame(){
    players.one.score = 0;
    players.two.score = 0;
    displayScore();
    document.querySelector("#logs").innerHTML = "";
}