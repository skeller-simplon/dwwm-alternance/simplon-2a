require('source-map-support/register');
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./books.json":
/*!********************!*\
  !*** ./books.json ***!
  \********************/
/*! exports provided: 0, 1, 2, 3, 4, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"name\":\"foo\",\"author\":\"ga\",\"date_publish\":\"01/03/1943\",\"price\":17},{\"name\":\"aaaa\",\"author\":\"bu\",\"date_publish\":\"01/03/1943\",\"price\":17},{\"name\":\"bbbb\",\"author\":\"zo\",\"date_publish\":\"01/03/1943\",\"price\":17},{\"name\":\"qkjef\",\"author\":\"me\",\"date_publish\":\"01/03/1943\",\"price\":17},{\"name\":\"mlsejgmes\",\"author\":\"foo\",\"date_publish\":\"01/03/1943\",\"price\":17}]");

/***/ }),

/***/ "./movies.json":
/*!*********************!*\
  !*** ./movies.json ***!
  \*********************/
/*! exports provided: 0, 1, 2, 3, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":1,\"title\":\"film 1\",\"director\":\"director1\",\"year\":\"year1\"},{\"id\":2,\"title\":\"film 1\",\"director\":\"director1\",\"year\":\"year1\"},{\"id\":3,\"title\":\"film 3\",\"director\":\"director3\",\"year\":\"year3\"},{\"id\":4,\"title\":\"film 4\",\"director\":\"director4\",\"year\":\"year4\"}]");

/***/ }),

/***/ "./routes/book.js":
/*!************************!*\
  !*** ./routes/book.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const BookRouter = express.Router();

const books = __webpack_require__(/*! ../books.json */ "./books.json");

BookRouter.get('/all', (req, res) => {
  res.json(books);
});
module.exports = BookRouter; // app.route('/book')
// .get((req, res) => {
//     res.send('voir un livre')
// })
// .post((req, res) => {
//     res.send('ajouter un livre')
// })
// .put((req, res) => {
//     res.send('modifier un livre');
// })

/***/ }),

/***/ "./routes/movie.js":
/*!*************************!*\
  !*** ./routes/movie.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const MovieRouter = express.Router();

const movies = __webpack_require__(/*! ../movies.json */ "./movies.json");

MovieRouter.get('/', (req, res) => {
  res.json(movies);
});
MovieRouter.get('/:id', (req, res) => {
  res.json(movies[req.params.id - 1]);
});
MovieRouter.post('/new', (req, res) => {
  let movie = req.body;
  res.json(movies.push(movie));
});
module.exports = MovieRouter;

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const express = __webpack_require__(/*! express */ "express");

const app = express();

const bodyParser = __webpack_require__(/*! body-parser */ "body-parser");

const port = 4567;

const BookRouter = __webpack_require__(/*! ../routes/book */ "./routes/book.js");

const MovieRouter = __webpack_require__(/*! ../routes/movie */ "./routes/movie.js");

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.listen(port, () => {
  console.log(`[server operationnel ici : ${port}]`);
});
app.get('/bonjour', (req, res) => {
  res.send('Sisi la famille !');
});
app.get('/hello/:prenom', (req, res) => {
  res.send('Salut ' + req.params.prenom);
});
app.use('/books', BookRouter);
app.use('/movies', MovieRouter);

/***/ }),

/***/ 0:
/*!****************************!*\
  !*** multi ./src/index.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/2a/node-scratch/src/index.js */"./src/index.js");


/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ })

/******/ });
//# sourceMappingURL=main.map