const express = require('express');
const BookRouter = express.Router();
const books = require('../books.json')

BookRouter.get('/all', (req, res) => {
    res.json(books)
})

module.exports = BookRouter
// app.route('/book')
// .get((req, res) => {
//     res.send('voir un livre')
// })
// .post((req, res) => {
//     res.send('ajouter un livre')
// })
// .put((req, res) => {
//     res.send('modifier un livre');
// })