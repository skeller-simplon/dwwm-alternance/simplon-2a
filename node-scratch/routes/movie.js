const express = require('express');
const MovieRouter = express.Router();
const movies = require('../movies.json')

MovieRouter.get('/', (req, res) => {
    res.json(movies)
})
MovieRouter.get('/:id', (req, res) => {
    res.json(movies[req.params.id - 1]);
})
MovieRouter.post('/new', (req, res) => {
    let movie = req.body;
    res.json(
        movies.push(movie)
    );
}) 

module.exports = MovieRouter