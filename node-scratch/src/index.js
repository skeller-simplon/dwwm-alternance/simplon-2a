const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = 4567;
const BookRouter = require('../routes/book')
const MovieRouter = require('../routes/movie')

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.listen(port, () => {
    console.log(`[server operationnel ici : ${port}]`)
})

app.get('/bonjour', (req, res) => {
    res.send('Sisi la famille !')
})

app.get('/hello/:prenom', (req, res) => {
    res.send('Salut '+ req.params.prenom)
})

app.use('/books', BookRouter);
app.use('/movies', MovieRouter);