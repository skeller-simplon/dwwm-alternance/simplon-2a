'use strict';

const request = require('request');

// Replace <Subscription Key> with your valid subscription key.
const subscriptionKey = '4ced491aae5141208004d37b4798c63d';

// You must use the same location in your REST call as you used to get your
// subscription keys. For example, if you got your subscription keys from
// westus, replace "westcentralus" in the URL below with "westus".
const uriBase = 'https://face-api-p9.cognitiveservices.azure.com/face/v1.0/detect';

const photosSam = [
    'https://www.simplonlyon.fr/promo9/skeller/images/skellerSimplon.jpg',
    "https://scontent-cdt1-1.xx.fbcdn.net/v/t1.0-9/52895331_10215145177755243_4347088184425840640_n.jpg?_nc_cat=111&_nc_oc=AQn3Nr0_XAhTQBH_gV78TknqOm9zFeSLXB6kFWplR54JsVviVRJYextI5Xqr5FbQ-bQ&_nc_ht=scontent-cdt1-1.xx&oh=a9e2f5b3b4087de9e68c8646d353e550&oe=5E469F9B",
    "https://scontent-cdt1-1.xx.fbcdn.net/v/t1.0-0/p206x206/1479312_10201780804054468_965281388_n.jpg?_nc_cat=110&_nc_oc=AQlLjI4BLWQdQrBE0p7gbZmUyqOh4lOYSWPAr_-W0sEvygsKQsqclKS6NuUSzAEG-o4&_nc_ht=scontent-cdt1-1.xx&oh=a34c1f4864c995af13c99840248f39d1&oe=5E5B5E29",
    "https://scontent-cdt1-1.xx.fbcdn.net/v/t31.0-8/737664_4300204498906_1820973283_o.jpg?_nc_cat=108&_nc_oc=AQkV-siEZhhtrH51lAASMSwR0nnpwsJxrPlF_3QfrhpqvbcU9mGCmJbWjmcqP-qPaAY&_nc_ht=scontent-cdt1-1.xx&oh=310692c3c31741028cadf0b4350934f1&oe=5E4AA6CC",
    "https://scontent-cdt1-1.xx.fbcdn.net/v/t1.0-9/292537_427065493988300_1299321255_n.jpg?_nc_cat=109&_nc_oc=AQkuxdgEr4eRRcxymc1y8jdzDZYqQVrLAh41iIwrOAz3Tm8dCwzh4caJGPJk5FfeaOQ&_nc_ht=scontent-cdt1-1.xx&oh=9c6306805203738a80bd72436cabbb15&oe=5E469B51",
    "https://scontent-cdt1-1.xx.fbcdn.net/v/t1.0-9/546431_427061207322062_657189446_n.jpg?_nc_cat=110&_nc_oc=AQkyqNKWNJDy3u5GzUhpNstGy5zqxSbHkZStlmCn3LJcGD4oFWk0ODeY9F1DuIurYII&_nc_ht=scontent-cdt1-1.xx&oh=fa215d5f92b534d7aee85c49aad8e100&oe=5E40673B",
    "https://scontent-cdt1-1.xx.fbcdn.net/v/t1.0-9/396496_1870105329952_60749061_n.jpg?_nc_cat=108&_nc_oc=AQlCx8Lf4KBaF7ysIMIgzqQ3B90DBmTM1DxJ3FwfeVkEdOWVLUvMfM1GPID646B92Aw&_nc_ht=scontent-cdt1-1.xx&oh=a3b2914edca269663d1b1c0a83a109ee&oe=5E59740A"
]
getPhotosIds(photosSam)

function getPhotosIds(photosLinks) {
   // Request parameters.
   const params = {
    'returnFaceId': 'true',
    'returnFaceLandmarks': 'false',
    'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,' +
        'emotion,hair,makeup,occlusion,accessories,blur,exposure,noise'
    };
    let array = [];
    for (const link of photosLinks) {
        const options = {
            uri: uriBase,
            qs: params,
            body: '{"url": ' + '"' + link + '"}',
            headers: {
                'Content-Type': 'application/json',
                'Ocp-Apim-Subscription-Key' : subscriptionKey
            }
        };
        request.post(options, (error, response, body) => {
          if (error) {
            console.log('Error: ', error);
            return;
          }
          let jsonResponse = JSON.stringify(JSON.parse(body), null, '  ');
        //   array.push(body[0].faceId)
          console.log(JSON.parse(body)[0].faceId);
        //   console.log(jsonResponse);
        });
    }
    console.log(array);
    
    
}
 