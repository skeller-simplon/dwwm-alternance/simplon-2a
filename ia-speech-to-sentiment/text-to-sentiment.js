let headers = new Headers();
myHeaders.append("Content-Type", "application/json");
myHeaders.append("Ocp-Apim-Subscription-Key", "53e2ed5a6afa4394bad401292d8114ff");


export default function translateTextToSentiment(text){
    let post = `{{
        "documents": [
          {
            "language": "en",
            "id": "1",
            "text": ${text}
          }
        ]
      }}`;
    
    return fetch("https://analyse-texte.cognitiveservices.azure.com/text/analytics/v2.1/sentiment", {
      method: "POST",
      headers: headers,
      body: JSON.parse(post)
    }).then(tes => tes)
}