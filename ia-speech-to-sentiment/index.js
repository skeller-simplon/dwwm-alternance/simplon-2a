// status fields and start button in UI
var phraseDiv;
var startRecognizeOnceAsyncButton;
// subscription key and region for speech services.
var subscriptionKey, serviceRegion;
var authorizationToken;
var SpeechSDK;
var recognizer;

document.addEventListener("DOMContentLoaded", function () {
  startRecognizeOnceAsyncButton = document.getElementById("startRecognizeOnceAsyncButton");
  subscriptionKey = document.getElementById("subscriptionKey");
  serviceRegion = document.getElementById("serviceRegion");
  phraseDiv = document.getElementById("phraseDiv");
  startRecognizeOnceAsyncButton.addEventListener("click", function () {
    startRecognizeOnceAsyncButton.disabled = true;
    phraseDiv.innerHTML = "";
    // if we got an authorization token, use the token. Otherwise use the provided subscription key
    var speechConfig;
    if (authorizationToken) {
      speechConfig = SpeechSDK.SpeechConfig.fromAuthorizationToken(authorizationToken, serviceRegion.value);
    } else {
      if (subscriptionKey.value === "" || subscriptionKey.value === "subscription") {
        alert("Please enter your Microsoft Cognitive Services Speech subscription key!");
        return;
      }
      speechConfig = SpeechSDK.SpeechConfig.fromSubscription(subscriptionKey.value, serviceRegion.value);
    }
    speechConfig.speechRecognitionLanguage = "fr-FR";
    var audioConfig  = SpeechSDK.AudioConfig.fromDefaultMicrophoneInput();
    recognizer = new SpeechSDK.SpeechRecognizer(speechConfig, audioConfig);
    recognizer.recognizeOnceAsync(
      function (result) {
        startRecognizeOnceAsyncButton.disabled = false;
        phraseDiv.innerHTML += result.text;
        // Appel à textToSentiment
        translateTextToSentiment(result.text);
        window.console.log(result);
        recognizer.close();
        recognizer = undefined;
      },
      function (err) {
        startRecognizeOnceAsyncButton.disabled = false;
        phraseDiv.innerHTML += err;
        window.console.log(err);
        recognizer.close();
        recognizer = undefined;
      });
  });
  if (!!window.SpeechSDK) {
    SpeechSDK = window.SpeechSDK;
    startRecognizeOnceAsyncButton.disabled = false;
    document.getElementById('content').style.display = 'block';
    document.getElementById('warning').style.display = 'none';
    // in case we have a function for getting an authorization token, call it.
    if (typeof RequestAuthorizationToken === "function") {
        RequestAuthorizationToken();
    }
  }
});




function translateTextToSentiment(text){

    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Ocp-Apim-Subscription-Key", "53e2ed5a6afa4394bad401292d8114ff");
    let post = {
        documents: [
        {
            "language": "fr",
            "id": "1",
            "text": text
          }
    ]};

    fetch("https://analyse-texte.cognitiveservices.azure.com/text/analytics/v2.1/sentiment", {
      method: "POST",
      headers: headers,
      body: JSON.stringify(post)
    }).then(tes => tes.json()).then(res => {
        console.log(res);
        phraseDiv.innerHTML += `\nLe sentiment est: ` + Math.round(res.documents[0].score * 100) / 100
        }
    )
}