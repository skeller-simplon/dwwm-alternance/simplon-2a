import azure.cognitiveservices.speech as speechsdk
import http.client, urllib.request, urllib.parse, urllib.error, base64
import requests


speech_key, service_region = "14d6e707cf6b475491c9fb117b7f3ba4", "francecentral"
locale = 'fr-CA'
speech_config = speechsdk.SpeechConfig(subscription=speech_key, region=service_region, speech_recognition_language=locale)

def speech_recognize_once_from_mic():
    """performs one-shot speech recognition from the default microphone"""
    # <SpeechRecognitionWithMicrophone>
    # Creates a speech recognizer using microphone as audio input.
    # The default language is "en-us".
    speech_recognizer = speechsdk.SpeechRecognizer(speech_config=speech_config)

    # Starts speech recognition, and returns after a single utterance is recognized. The end of a
    # single utterance is determined by listening for silence at the end or until a maximum of 15
    # seconds of audio is processed. It returns the recognition text as result.
    # Note: Since recognize_once() returns only a single utterance, it is suitable only for single
    # shot recognition like command or query.
    # For long-running multi-utterance recognition, use start_continuous_recognition() instead.
    result = speech_recognizer.recognize_once()

    # Check the result
    if result.reason == speechsdk.ResultReason.RecognizedSpeech:
        print("Recognized: {}".format(result.text))
        return result.text
    elif result.reason == speechsdk.ResultReason.NoMatch:
        print("No speech could be recognized")
    elif result.reason == speechsdk.ResultReason.Canceled:
        cancellation_details = result.cancellation_details
        print("Speech Recognition canceled: {}".format(cancellation_details.reason))
        if cancellation_details.reason == speechsdk.CancellationReason.Error:
            print("Error details: {}".format(cancellation_details.error_details))
    return "error"
    # </SpeechRecognitionWithMicrophone>

def sentiment_analysis(data):
    sentiment_url = "https://analyse-texte.cognitiveservices.azure.com/text/analytics/v2.1/sentiment"
    documents = {"documents": [
    {"id": "1", "language": locale,
        "text": data},
    ]}
    headers = {"Ocp-Apim-Subscription-Key": "53e2ed5a6afa4394bad401292d8114ff"}
    response = requests.post(sentiment_url, headers=headers, json=documents)
    sentiments = response.json()
    return float(sentiments["documents"][0]["score"])

def speech_synthesis_with_language(score):
    """performs speech synthesis to the default speaker with specified spoken language"""
    # Creates an instance of a speech config with specified subscription key and service region.
    speech_config = speechsdk.SpeechConfig(subscription=speech_key, region=service_region)

    # Language setting - See all possible languages:
    # https://docs.microsoft.com/azure/cognitive-services/speech-service/language-support#text-to-speech
    language = locale

    speech_config.speech_synthesis_language = language
    # Creates a speech synthesizer for the specified language,
    # using the default speaker as audio output.
    speech_synthesizer = speechsdk.SpeechSynthesizer(speech_config=speech_config)

    list_answers = ["Tu es si heureux!", "Tu es sans sentiment", "Tu es en colère"]
    if score<0.4:
        data = list_answers[2]
    elif score>0.6:
        data = list_answers[0]
    else:
        data = list_answers[1]

    text=data 

    result = speech_synthesizer.speak_text_async(text).get()
    # Check result
    if result.reason == speechsdk.ResultReason.SynthesizingAudioCompleted:
        print("Speech synthesized to speaker for text [{}] with language [{}]".format(text, language))
    elif result.reason == speechsdk.ResultReason.Canceled:
        print("Speech synthesis canceled: {}".format(result.cancellation_details.reason))
    if result.reason == speechsdk.CancellationReason.Error:
        print("Error details: {}".format(cancellation_details.error_details))

# Creates a speech synthesizer using the default speaker as audio output.
speech_synthesizer = speechsdk.SpeechSynthesizer(speech_config=speech_config)

# Receives a text from voice.
print("Il faut parler maintenant.")
result = speech_recognize_once_from_mic()
score = sentiment_analysis(result)
print("L'intention est [{}]".format(score))
speech_synthesis_with_language(score)