const letters = [
    {letter: 'A', isPressed: false},
    {letter: 'B', isPressed: false},
    {letter: 'C', isPressed: false},
    {letter: 'D', isPressed: false},
    {letter: 'E', isPressed: false},
    {letter: 'F', isPressed: false},
    {letter: 'G', isPressed: false},
    {letter: 'H', isPressed: false},
    {letter: 'I', isPressed: false},
    {letter: 'J', isPressed: false},
    {letter: 'K', isPressed: false},
    {letter: 'L', isPressed: false},
    {letter: 'M', isPressed: false},
    {letter: 'N', isPressed: false},
    {letter: 'O', isPressed: false},
    {letter: 'P', isPressed: false},
    {letter: 'Q', isPressed: false},
    {letter: 'R', isPressed: false},
    {letter: 'S', isPressed: false},
    {letter: 'T', isPressed: false},
    {letter: 'U', isPressed: false},
    {letter: 'V', isPressed: false},
    {letter: 'W', isPressed: false},
    {letter: 'X', isPressed: false},
    {letter: 'Y', isPressed: false},
    {letter: 'Z', isPressed: false}
];



const game = {
    word: '',
    wordGuessState: '',
    attemps: 0
};




setupGame();

function setupGame(){
    game.word = prompt('Il est l\'heure de choisir un mot').toUpperCase();
    game.wordGuessState =  "_".repeat(game.word.length);
    game.attemps = 0;

    updateDisplay();
    setupKeyboard();
};


function checkLetterInWord(letter){
    let tempVar = "";
    let isInWord = false;
    for (let i = 0; i < game.word.length; i++) {
        if (game.word[i] === letter) {
            tempVar += letter;
            isInWord = true;
        }else{
            tempVar += game.wordGuessState[i];
        }
    }
    if (!isInWord) {
        game.attemps++;        
    }
    game.wordGuessState = tempVar;
    updateDisplay();
    checkForWin();
};

function checkForWin(){
    if (game.attemps === 10) {
        alert('GAME OVER, le mot était: '+ game.word);
        setupGame();
    }
    else if (!game.wordGuessState.includes('_')) {
        alert('WON BITCHES');
    }
}

function updateDisplay(){
    const wordSelector = document.querySelector("#word");
    wordSelector.innerHTML = game.wordGuessState;
    const scoreSelector = document.querySelector("#score");
    scoreSelector.innerHTML = game.attemps.toString();
}

function setupKeyboard(){
    const selector = document.querySelector('#letters')
    selector.innerHTML = "";
    letters.forEach(element => {
        const btnDiv = document.createElement('div');
        const btn = document.createElement('button')
        btn.classList.add('btn', 'btn-light');
        btn.innerHTML = element.letter.toUpperCase();
        btnDiv.classList.add('col-1');
        btn.addEventListener('click', () => {
            if (!element.isPressed) {
                clickedLetterEvent(element);
                btn.disabled = true;
            }
        });
        selector.appendChild(btnDiv);
        btnDiv.appendChild(btn);
    });
}

function clickedLetterEvent(element){
    const index = letters.indexOf(element);
    letters[index].isPressed = true;
    checkLetterInWord(element.letter);
}