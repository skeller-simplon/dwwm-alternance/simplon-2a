const express = require("express");
const app = express();
const volleyball = require('volleyball');
const router = require("./routes/index")
const mongoose = require('mongoose');
require('dotenv').config();
mongoose.connect('mongodb://localhost/my_db', {useNewUrlParser: true})

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log(`[MongoDB is running here 🤓]`)
  // we're connected!
});

const {DB_port} = process.env;
app.use(volleyball);

app.use(express.urlencoded({extended: false}))
app.use(express.json())

app.use('/', router);


app.listen(DB_port, () => {
    console.log([`Mon app tourne bien: port ${DB_port}`])
})