const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FoodTruckSchema = new Schema({
    name: {type: String, required: true},
    foodType: {type: String, required: true}
})

const FoodTruck = new mongoose.model('FoodTruck', FoodTruckSchema);

module.exports = FoodTruck