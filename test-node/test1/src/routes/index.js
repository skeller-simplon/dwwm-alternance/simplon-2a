const express = require("express");
const router = express();
const basicRouter = require("./basic-routes")
const ftRouter = require("./foodtruckRoute")

router.use("/", basicRouter);

router.use("/food", ftRouter)

module.exports = router;