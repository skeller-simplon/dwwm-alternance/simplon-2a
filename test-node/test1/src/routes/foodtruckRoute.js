const express = require('express');
const FoodTruck = require ('../models/foodtruck')

const ftRouter = express.Router();

ftRouter.get('/all', (req, res) => {
    FoodTruck.find({}, (err, foodtrucks) => {
        if (err) console.error(err)
        res.json(foodtrucks)
    });
})

ftRouter.route('/:id')
    .get((req, res) => {
        FoodTruck.find({_id: req.params.id}, (err, foodtruck) => {
            if (err) console.error(err)
            res.json({ message: `GET - Récupération de la ressource d'id: ${foodtruck._id}`, data: foodtruck})
        });
    }).patch((req, res) => {
        FoodTruck.findById({_id: req.params.id}, (err, foodtruck) => {
            if (err) console.error(err)
            console.log(foodtruck)
            Object.assign(foodtruck, req.body).save((err, foodtruck) => {
                if (err) console.error(err)
                res.json({message: `EDIT - Modification de la ressource d'id: ${foodtruck._id}`, data: foodtruck})
            })
        });
    }).delete((req, res) => {
        FoodTruck.remove({_id: req.params.id}, (err, foodtruck) => {
            res.json({message: `REMOVE - Suppression de la ressource d'id: ${foodtruck._id}`, data: foodtruck})
        })
    })


ftRouter.post('/add', (req, res) => {
    const newFoodTruck = new FoodTruck(req.body);
    newFoodTruck.save((err, foodtrucks) => {
        if (err) console.error(err)
        res.json(foodtrucks)
    })
})

module.exports = ftRouter